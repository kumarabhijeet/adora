angular
  .module("adora.controllers", [])
  .controller("LoginCtrl", function(
    $scope,
    $state,
    LoginFactory,
    SocketService,
    environmentVariables,
    $http,
    SQLiteService,
    MQTTService
  ) {
    $scope.login = { email: "", password: "" };

    $scope.loginResponse = {};

    $scope.tryLogin = function() {
      MQTTService.connect($scope.login.email, $scope.login.password, false);
    };

    var loginResponseCallback = function() {
      if ($scope.login.email.trim() !== "") {
        $http.get(environmentVariables.ADORA_API_URL + "/users/authenticate?email=" + $scope.login.email).then(
          function(userDetails) {
            if (userDetails !== undefined && userDetails.data !== undefined && userDetails.data.email !== undefined) {
              if (
                SQLiteService.insert(
                  "INSERT INTO user_info (id, user_id, user_email, password, status) VALUES (NULL, ?, ?, ?, 1)",
                  [userDetails.data.id, userDetails.data.email, $scope.login.password]
                ) !== false
              ) {
                $state.go("tab.home");
              } else {
                $state.go("error", { state: "login" });
              }
            } else {
              $state.go("error", { state: "login" });
            }
          },
          function(response) {
            //$state.go("error", { state: "login" });
          }
        );
      } else {
        $state.go("tab.home");
      }
    };

    $scope.$on("login", function() {
      loginResponseCallback();
    });
  })
  .controller("ErrorCtrl", function($scope, $state, $stateParams) {
    $scope.retry = function() {
      //$state.go('')
    };
  })
  .controller("SignUpCtrl", function($scope, $state, SocketService, environmentVariables, $http) {
    $scope.signup = { userName: "", email: "", password: "", confirmPassword: "" };

    var checkPasswordConfirmation = function() {
      if ($scope.signup.password === $scope.signup.confirmPassword) {
        return true;
      }
      return false;
    };

    $scope.trySignup = function() {
      if (checkPasswordConfirmation() === true) {
        SocketService.openSocket(signUpResponseCallback, function() {});
        var data = {
          method: "register",
          username: $scope.signup.userName,
          email: $scope.signup.email,
          password: $scope.signup.password
        };
        data = JSON.stringify(data);
        SocketService.writeSocket(data);
      }
    };

    var signUpResponseCallback = function(data) {
      SocketService.printResponse(data);
      if (data !== undefined && data.method === "register" && data.value === "success") {
        var postData =
          "userName=" +
          $scope.signup.userName +
          "&email=" +
          $scope.signup.email +
          "&password=" +
          $scope.signup.password;
        $http
          .post(environmentVariables.ADORA_API_URL + "/users", postData, {
            headers: { "Content-Type": "application/x-www-form-urlencoded" }
          })
          .then(
            function(response) {
              SocketService.printResponse(response);
              $state.go("login");
            },
            function(response) {
              console.log("HTTP failed");
              SocketService.printResponse(response);
              $state.go("error", { state: "signup" });
            }
          );
      } else if (data.method == "register" && data.dueto !== undefined) {
        alert(data.dueto);
      } else {
        $state.go("error", { state: "signup" });
      }
    };
  })
  .controller("LayoutCtrl", function(
    $scope,
    $http,
    $state,
    $rootScope,
    $ionicPopover,
    SQLiteService,
    $ionicPlatform,
    MQTTService,
    LoginFactory,
    $ionicHistory,
    $ionicViewSwitcher
  ) {
    $ionicPopover
      .fromTemplateUrl("templates/menupopover.html", {
        scope: $rootScope
      })
      .then(function(popover) {
        $rootScope.menupopover = popover;
      });

    $scope.goTo = function(tab) {
      switch (tab) {
        case "home":
          $state.go("tab.home");
          break;
        case "favorites":
          $state.go("tab.favorites");
          break;
        case "clubbed-devices":
          $state.go("tab.clubbed-devices");
          break;
        case "routines":
          $state.go("tab.routines");
          break;
      }
    };

    $scope.hidePopover = function() {
      $rootScope.menupopover.hide();
    };

    $rootScope.logout = function() {
      SQLiteService.logout().then(
        function(result) {
          $state.go("welcome");
        },
        function(error) {}
      );
    };

    $scope.getSwitchedOnList = function() {};

    $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
      $rootScope.menupopover.hide();
    });

    $rootScope.goBack = function() {
      console.log("going back");
      $ionicViewSwitcher.nextDirection("backward");
      //$ionicHistory.goBack();
    };

    $ionicPlatform.on("resume", function() {
      console.log("Resuming operations");
      LoginFactory.checkLogin().then(
        function(response) {
          $rootScope.loginDetails = response;
          MQTTService.connect($rootScope.loginDetails.user_email, $rootScope.loginDetails.password, true);
        },
        function(error) {
          console.log(error);
        }
      );
    });

    $rootScope.verticalMenuOptions = true;
  })
  .controller("HomeCtrl", function(
    $scope,
    $http,
    LoginFactory,
    $state,
    environmentVariables,
    SocketService,
    $rootScope,
    $ionicPopover,
    SQLiteService,
    UtilityFactory,
    $ionicActionSheet,
    $timeout
  ) {
    $scope.userRooms = [];
    $scope.showUserRooms = false;
    $rootScope.loginDetails = {};

    var getUserRooms = function() {
      $http.get(environmentVariables.ADORA_API_URL + "/user-rooms/" + $rootScope.loginDetails.user_id).then(
        function(response) {
          $scope.userRooms = response.data.userRooms;
          $scope.showUserRooms = true;
          UtilityFactory.adjustTileHeight();
        },
        function(response) {
          SocketService.printResponse(response);
        }
      );
    };

    $scope.deleteUserRoom = function(roomId) {
      $http.delete(environmentVariables.ADORA_API_URL + "/user-rooms/" + roomId).then(
        function(response) {
          $state.go("tab.home", {}, { reload: true });
        },
        function(error) {
          console.log(error);
        }
      );
    };

    $scope.showActionSheet = function(roomIndex) {
      var hideSheet = $ionicActionSheet.show({
        destructiveText: "Delete",
        titleText: $scope.userRooms[roomIndex].userRoomName,
        cancelText: "Cancel",
        destructiveButtonClicked: function() {
          $scope.deleteUserRoom($scope.userRooms[roomIndex].id);
        }
      });

      $timeout(function() {
        hideSheet();
      }, 10000);
    };

    LoginFactory.checkLogin().then(
      function(response) {
        $rootScope.loginDetails = response;
        getUserRooms();
      },
      function(error) {
        console.log(error);
      }
    );

    $rootScope.verticalMenuOptions = { home: true };
  })
  .controller("AddRoomCtrl", function($scope, LoginFactory, $state, $http, environmentVariables, $rootScope) {
    $scope.rooms = [];
    $scope.showRooms = false;

    $scope.getRooms = function() {
      $http.get(environmentVariables.ADORA_API_URL + "/rooms").then(function(response) {
        $scope.rooms = response.data.rooms;
        $scope.showRooms = true;
      });
    };

    $scope.addRoom = function(roomId, roomName) {
      var postData = "userId=" + $rootScope.loginDetails.user_id + "&roomName=" + roomName + "&roomId=" + roomId;
      $http
        .post(environmentVariables.ADORA_API_URL + "/user-rooms", postData, {
          headers: { "Content-Type": "application/x-www-form-urlencoded" }
        })
        .then(function(response) {
          if (response.data.status === "success") {
            $state.go("tab.room-detail", { userRoomId: response.data.id, userRoomName: response.data.name });
          } else {
            // error
          }
        });
    };

    LoginFactory.checkLogin().then(
      function(response) {
        $rootScope.loginDetails = response;
        $scope.getRooms();
      },
      function(error) {
        console.log(error);
      }
    );
  })
  .controller("RoomDetailCtrl", function(
    $scope,
    $stateParams,
    LoginFactory,
    $state,
    $http,
    environmentVariables,
    SocketService,
    UtilityFactory,
    $rootScope,
    MQTTService,
    $ionicActionSheet,
    $timeout
  ) {
    $scope.room = { id: $stateParams.userRoomId, name: $stateParams.userRoomName };
    $scope.switchedOnDevicesReady = false;
    var switchedOnListAttempts = 0;
    $rootScope.verticalMenuOptions = { addRoomItem: true };

    var getRoomInfo = function() {
      $http
        .get(environmentVariables.ADORA_API_URL + "/user-room-items/" + $stateParams.userRoomId)
        .then(function(response) {
          if (response.data.status === "success") {
            if (response.data.roomItems.length > 0) {
              $scope.roomItems = response.data.roomItems;
              for (var i = 0; i < $scope.roomItems.length; i++) {
                var roomItem = $scope.roomItems[i];
                roomItem.status = 0;
                var lastOccurenceOfDot = roomItem.icon !== undefined ? roomItem.icon.lastIndexOf(".") : -1;
                if (lastOccurenceOfDot > -1) {
                  roomItem.onIcon =
                    roomItem.icon.substring(0, lastOccurenceOfDot) +
                    "-on" +
                    roomItem.icon.substring(lastOccurenceOfDot);
                }
                $scope.roomItems[i] = roomItem;
              }
              UtilityFactory.adjustTileHeight();
              updateSwitchStatuses();
            } else {
              $scope.roomItems = false;
            }
          }
        });
    };

    var updateSwitchStatuses = function() {
      if ($scope.roomItems === undefined || $scope.switchedOnDevicesReady === false) {
        switchedOnListAttempts++;
        if (switchedOnListAttempts <= 5) {
          updateSwitchStatuses();
        }
      } else {
        if ($scope.switchedOnDevices !== undefined) {
          for (var i = 0; i < $scope.roomItems.length; i++) {
            var roomItem = $scope.roomItems[i];
            $scope.roomItems[i].status = $scope.switchedOnDevices[roomItem.device_id + ":" + roomItem.switch_number];
          }
        }
      }
    };

    var getSwitchedOnList = function() {
      $http.get(environmentVariables.ADORA_API_URL + "/device-statuses?userId=" + $rootScope.loginDetails.user_id).then(
        function(response) {
          var switchedOnDevicesRaw = response.data.switchedOnDevices;
          $scope.switchedOnDevices = {};
          for (var i = 0; i < switchedOnDevicesRaw.length; i++) {
            $scope.switchedOnDevices[switchedOnDevicesRaw[i].device_id + ":" + switchedOnDevicesRaw[i].switch_number] =
              switchedOnDevicesRaw[i].status == "1" ? true : false;
          }
          $scope.switchedOnDevicesReady = true;
          updateSwitchStatuses();
        },
        function(response) {
          SocketService.printResponse(response);
        }
      );
    };

    $scope.switchDevice = function(index) {
      var item = $scope.roomItems[index];
      item.status = !item.status;
      UtilityFactory.createSwitchStatus({
        userId: $rootScope.loginDetails.user_id,
        deviceId: item.device_id,
        switchNumber: item.switch_number,
        status: item.status ? 1 : 0,
        userRoomItemId: item.id
      });
      MQTTService.publish(
        {
          method: "switch",
          device_uid: item.device_id,
          switch_nos: item.switch_number,
          action: item.status ? "ON" : "OFF",
          eventCallback: "switch"
        },
        "a/" + $rootScope.loginDetails.user_email + "/r/req"
      );
    };

    var switchSuccess = function(response) {
      SocketService.printResponse(response);
      if (response !== undefined && response.method === "switch" && response.value === "success") {
        //UtilityFactory.createSwitchStatus({deviceId: response.id, switchNumber: response.switchno, status: $scope.item.status === 'on' ? 1 : 0});
      } else if (response !== undefined && response.method === "switch" && response.value === "failed") {
        alert(response.dueto);
      } else {
        console.log(error);
        $state.go("error", { state: "login" });
      }
    };

    $scope.onHold = function(itemIndex) {
      var hideSheet = $ionicActionSheet.show({
        destructiveText: "Delete",
        titleText: $scope.roomItems[itemIndex].name,
        cancelText: "Cancel",
        destructiveButtonClicked: function() {
          deleteUserRoomItem($scope.roomItems[itemIndex].id);
        }
      });

      $timeout(function() {
        hideSheet();
      }, 10000);
    };

    var deleteUserRoomItem = function(userRoomItemId) {
      $http.delete(environmentVariables.ADORA_API_URL + "/user-room-items/" + userRoomItemId).then(
        function(response) {
          $state.go(
            "tab.room-detail",
            { userRoomId: $scope.room.id, userRoomName: $scope.room.name },
            { reload: true }
          );
        },
        function(error) {
          console.log(error);
        }
      );
    };

    $scope.addRoomItem = function() {
      $state.go("tab.fill-room", { userRoomId: $scope.room.id });
    };

    LoginFactory.checkLogin().then(
      function(response) {
        $rootScope.loginDetails = response;
        getRoomInfo();
        getSwitchedOnList();
      },
      function(error) {
        console.log(error);
      }
    );
  })
  .controller("ItemDetailCtrl", function(
    $scope,
    $stateParams,
    LoginFactory,
    $state,
    $http,
    environmentVariables,
    SocketService,
    UtilityFactory,
    $rootScope,
    MQTTService
  ) {
    $scope.item = {
      id: $stateParams.userRoomItemId,
      name: $stateParams.userRoomItemName,
      uniqueId: "",
      userRoomId: "",
      status: "",
      switchNumber: "",
      masterId: "",
      slaveId: "",
      rule: ""
    };
    $scope.uniqueId = "";
    $scope.isSensor = false;
    $scope.slaveBound = false;

    var getUserRoomItemInfo = function() {
      $http
        .get(environmentVariables.ADORA_API_URL + "/user-room-items/item?userRoomItemId=" + $scope.item.id)
        .then(function(response) {
          if (response.data.status === "success") {
            if (response.data.userRoomItem.room_item_id === "4") {
              $scope.isSensor = true;
            }
            $scope.uniqueId = $scope.item.uniqueId = $scope.item.masterId = response.data.userRoomItem.device_id;
            $scope.item.userRoomId = response.data.userRoomItem.user_room_id;
            $scope.item.slaveId = response.data.userRoomItem.slave_id;
            $scope.item.switchNumber = response.data.userRoomItem.switch_number;
            if ($scope.item.slaveId !== "") {
              $scope.slaveBound = true;
            }
            if ($scope.item.uniqueId === "") {
              $scope.uniqueIdError = true;
            } else {
              $scope.uniqueIdEntered = true;
            }
            getSwitchStatus();
          }
        });
    };

    /*var getTypeSelected = function() {
    var typeRadioList = document.querySelectorAll('[name="type"]');
    for(var i = 0; i < typeRadioList.length; i++) {
      if(typeRadioList[i].checked === true) {
        return typeRadioList[i].value;
      }
    }
    return false;
  };*/

    $scope.attachDevice = function() {
      if ($scope.item.uniqueId !== "") {
        MQTTService.publish(
          {
            method: "attach",
            device_uid: $scope.item.uniqueId,
            eventCallback: "attach"
          },
          "a/" + $rootScope.loginDetails.user_email + "/r/req"
        );
      }
    };

    var getSwitchStatus = function() {
      SocketService.openSocket(switchStatusCallback, function() {});
      var data = { method: "switchSync", id: $scope.item.uniqueId };
      data = JSON.stringify(data);
      SocketService.writeSocket(data);
    };

    var switchStatusCallback = function(response) {
      if (response !== undefined && response.method === "switchSync" && response.value === "success") {
        if (response.state === "0" || response.state === 0) {
          $scope.item.status = "off";
        } else if (response.state === "1" || response.state === 1) {
          $scope.item.status = "on";
        }
      } else if (response !== undefined && response.value === "failed") {
        alert(response.dueto);
      } else {
        SocketService.printResponse(response);
        $state.go("error");
      }
    };

    var saveUniqueId = function(response) {
      if ($scope.item.uniqueId.trim() === "") {
        return false;
      }
      if (response !== undefined) {
        console.log(JSON.stringify(response));
      }
      var postData =
        "userRoomId=" +
        $scope.item.userRoomId +
        "&roomItemId=" +
        $scope.item.id +
        "&deviceId=" +
        $scope.item.uniqueId +
        "&switchNumber=" +
        ($scope.item.switchNumber !== "" ? $scope.item.switchNumber : 1);
      $http
        .post(environmentVariables.ADORA_API_URL + "/user-room-items", postData, {
          headers: { "Content-Type": "application/x-www-form-urlencoded" }
        })
        .then(
          function(response) {
            if (response.data.status === "success") {
              $scope.uniqueId = $scope.item.masterId = $scope.item.uniqueId;
              $scope.uniqueIdError = false;
            } else if (response.data.status === "failure" && response.data.reason === "duplicate") {
              alert("You have already used this device id / switch number.");
            }
          },
          function(error) {
            console.log(error);
          }
        );
    };

    $scope.switch = function() {
      if ($scope.uniqueId !== $scope.item.uniqueId) {
        // tell user he hasnt saved changes
        alert("save changes");
      } else {
        //UtilityFactory.createSwitchStatus({deviceId: item.device_id, switchNumber: item.switch_number, status: item.status});
        var switchNumber = 1; // doubt
        MQTTService.publish(
          {
            method: "switch",
            device_uid: $scope.item.uniqueId,
            switch_nos: $scope.item.switchNumber.trim() !== "" ? $scope.item.switchNumber : 1,
            action: $scope.item.status === "on" ? "OFF" : "ON",
            eventCallback: "switchInItemDetail"
          },
          "a/" + $rootScope.loginDetails.user_email + "/r/req"
        );
      }
    };

    var switchSuccess = function(response) {
      console.log("device was switched");
    };

    var saveSlaveId = function(response) {
      var save = false;
      if (response !== undefined && response.method === "bind" && response.value === "success") {
        SocketService.printResponse(response);
        save = true;
      } else if (response !== undefined && response.method === "switch" && response.value === "failed") {
        SocketService.printResponse(response);
        alert(response.dueto);
      } else if (response === undefined) {
        save = true;
      } else {
        console.log(error);
        $state.go("error", { state: "login" });
      }
      if (save) {
        $scope.slaveBound = true;
        var postData = "roomItemId=" + $scope.item.id + "&slaveId=" + $scope.item.slaveId;
        $http
          .post(environmentVariables.ADORA_API_URL + "/user-room-items", postData, {
            headers: { "Content-Type": "application/x-www-form-urlencoded" }
          })
          .then(function(response) {
            if (response.data.status === "success") {
              //
            }
          });
      }
    };

    $scope.bindDevice = function() {
      if ($scope.item.masterId !== "" && $scope.item.slaveId !== "") {
        $scope.masterSlaveError = false;
        MQTTService.publish(
          {
            method: "bind",
            master_id: $scope.item.masterId,
            slave_id: $scope.item.slaveId,
            eventCallback: "bind"
          },
          "a/" + $rootScope.loginDetails.user_email + "/r/req"
        );
      } else {
        $scope.masterSlaveError = true;
      }
    };

    $scope.setSensorRule = function() {
      if (window.cordova) {
        if (typeof $scope.item.rule === "Number") {
          $scope.item.rule = $scope.item.rule.toString();
        }
        SocketService.openSocket(setRuleSuccess, function() {});
        var data = { method: "ruleSet", id: $scope.item.masterId, occupied: "True", state: $scope.item.rule };
        data = JSON.stringify(data);
        SocketService.printResponse(data);
        SocketService.writeSocket(data);
      }
    };

    var setRuleSuccess = function(response) {
      if (response !== undefined && response.method === "ruleSet" && response.value === "success") {
        alert("Rule set");
      } else if (response !== undefined && response.method === "ruleSet" && response.value !== "success") {
        alert(response.dueto);
      } else {
        SocketService.printResponse(response);
        console.log(response);
        $state.go("error", { state: "login" });
      }
    };

    LoginFactory.checkLogin().then(
      function(response) {
        $rootScope.loginDetails = response;
        getUserRoomItemInfo();
      },
      function(error) {
        console.log(error);
      }
    );

    $scope.$on("attach", function() {
      console.log("attached");
      saveUniqueId();
    });

    $scope.$on("bind", function() {
      console.log("bound");
      saveSlaveId();
    });

    $scope.$on("switchInItemDetail", function() {
      console.log("switched");
    });
  })
  .controller("FillRoomCtrl", function($scope, LoginFactory, $state, $http, environmentVariables, $stateParams) {
    $scope.items = [];
    $scope.showItems = false;
    $scope.userRoomId = $stateParams.userRoomId;

    $scope.getItems = function() {
      $http.get(environmentVariables.ADORA_API_URL + "/room-items").then(function(response) {
        $scope.items = response.data.items;
        $scope.showItems = true;
      });
    };

    $scope.addItem = function(roomItemId, roomItemName) {
      var postData =
        "userRoomId=" + $stateParams.userRoomId + "&roomItemId=" + roomItemId + "&roomItemName=" + roomItemName;
      $http
        .post(environmentVariables.ADORA_API_URL + "/user-room-items", postData, {
          headers: { "Content-Type": "application/x-www-form-urlencoded" }
        })
        .then(function(response) {
          if (response.data.status === "success") {
            $state.go("tab.item-detail", { userRoomItemId: response.data.id, userRoomItemName: response.data.name });
          } else {
            // error
          }
        });
    };

    $scope.getItems();
  })
  .controller("MapDevicesCtrl", function(
    $scope,
    $state,
    LoginFactory,
    $stateParams,
    environmentVariables,
    $ionicPopup,
    $http
  ) {
    LoginFactory.checkLogin();
    $scope.device = { itemId: $stateParams.itemId, itemName: $stateParams.itemName, id: "", name: "" };

    $scope.saveDevice = function() {
      var postData =
        "userId=" +
        localStorage.getItem("userId") +
        "&userRoomId=" +
        $stateParams.userRoomId +
        "&deviceId=" +
        $scope.device.id +
        "&deviceName=" +
        $scope.device.name +
        "&roomItemId=" +
        $scope.device.itemId;

      $http
        .post(environmentVariables.ADORA_API_URL + "/user-room-items", postData, {
          headers: { "Content-Type": "application/x-www-form-urlencoded" }
        })
        .then(function(response) {
          if (response.data.status === "success") {
            $state.go("tab.dashboard");
          }
        });
    };
  })
  .controller("RoomItemsCtrl", function($scope, LoginFactory, $rootScope, environmentVariables, $http, $stateParams) {
    $scope.roomItems = [];
    $scope.showRoomItems = false;
    $scope.userRoomId = $stateParams.userRoomId;

    var getRoomItems = function() {
      $http
        .get(
          environmentVariables.ADORA_API_URL +
            "/user-room-items/" +
            $scope.userRoomId +
            "?userId=" +
            $rootScope.loginDetails.user_id
        )
        .then(function(response) {
          if (response.data.status === "success") {
            $scope.roomItems = response.data.roomItems;
            $scope.showRoomItems = true;
          }
        });
    };

    $scope.getStatus = function(deviceId) {
      return true;
    };

    $scope.switch = function(deviceId) {
      //console.log(deviceId);
    };

    LoginFactory.checkLogin().then(
      function(response) {
        $rootScope.loginDetails = response;
        getRoomItems();
      },
      function(error) {
        console.log(error);
      }
    );
  })
  .controller("FavoritesCtrl", function(
    $scope,
    LoginFactory,
    $http,
    environmentVariables,
    $rootScope,
    SocketService,
    $state,
    UtilityFactory,
    MQTTService,
    $timeout
  ) {
    $scope.favorites = [];
    $rootScope.loginDetails = {};
    $scope.showFavorites = false;

    LoginFactory.checkLogin().then(
      function(response) {
        $rootScope.loginDetails = response;
        getFavorites();
      },
      function(error) {
        console.log(error);
      }
    );

    $scope.fillDeviceIdAlert = function(itemIndex) {
      alert("No device id.");
      $scope.favorites[itemIndex].status = !$scope.favorites[itemIndex].status;
    };

    $scope.switchDevice = function(index) {
      var aFavorite = $scope.favorites[index];
      var status = aFavorite.status ? 0 : 1;
      aFavorite.status = !aFavorite.status;
      UtilityFactory.createSwitchStatus({
        userId: $rootScope.loginDetails.user_id,
        deviceId: aFavorite.deviceId,
        switchNumber: aFavorite.switchNumber,
        status: status,
        userRoomItemId: aFavorite.userRoomItemId
      });
      MQTTService.publish(
        {
          method: "switch",
          device_uid: aFavorite.deviceId,
          switch_nos: aFavorite.switchNumber,
          action: status ? "ON" : "OFF",
          eventCallback: "switchFavorites"
        },
        "a/" + $rootScope.loginDetails.user_email + "/r/req"
      );
    };

    var switchSuccess = function(response) {
      console.log("switched");
    };

    var getFavorites = function() {
      $http.get(environmentVariables.ADORA_API_URL + "/user-favorites?userId=" + $rootScope.loginDetails.user_id).then(
        function(response) {
          $scope.favorites = response.data.userFavorites;
          if ($scope.favorites.length > 0) {
            $rootScope.verticalMenuOptions = { changeFavorites: true };
          } else {
            $rootScope.verticalMenuOptions = { addFavorites: true };
          }
          for (var i = 0; i < $scope.favorites.length; i++) {
            var favoriteDetail = $scope.favorites[i];
            if (favoriteDetail.userRoomItemId === 0) {
              favoriteDetail.itemDetailParams = {
                userRoomItemId: favoriteDetail.userRoomItemId,
                userRoomItemName: favoriteDetail.userRoomItemName,
                userRoomName: favoriteDetail.userRoomName
              };
              favoriteDetail.roomItemIcon = favoriteDetail.roomItemIcon;
            } else {
              favoriteDetail.roomDetailParams = {
                userRoomId: favoriteDetail.userRoomId,
                userRoomName: favoriteDetail.userRoomName
              };
              favoriteDetail.roomIcon = favoriteDetail.roomIcon;
            }
            favoriteDetail.status = 0;
            $scope.favorites[i] = favoriteDetail;
          }
          UtilityFactory.adjustTileHeight();
          $scope.showFavorites = true;
        },
        function(error) {
          console.log(error);
        }
      );
    };

    $scope.showRoomItems = function(favorite) {
      if (favorite.userRoomItemId === "0") {
        $state.go("tab.favorite-room-detail", favorite.roomDetailParams);
      }
    };

    $scope.$on("switchFavorites", function() {
      switchSuccess();
    });
  })
  .controller("AddFavoriteCtrl", function(
    $scope,
    LoginFactory,
    $http,
    environmentVariables,
    $rootScope,
    UtilityFactory,
    $state,
    MQTTService
  ) {
    $scope.switchedOnDevices = [];

    var getRoomsAndItems = function() {
      $http
        .get(
          environmentVariables.ADORA_API_URL +
            "/user-room-items?userId=" +
            $rootScope.loginDetails.user_id +
            "&itemInfo=true"
        )
        .then(function(response) {
          if (response.data.status === "success") {
            $scope.roomsAndItems = response.data.roomsAndItems;
            $scope.roomsAsFavorites = {};
            getFavorites();
          }
        });
    };

    var getFavorites = function() {
      $http.get(environmentVariables.ADORA_API_URL + "/user-favorites?userId=" + $rootScope.loginDetails.user_id).then(
        function(response) {
          $scope.favorites = response.data.userFavorites;
          if ($scope.favorites.length > 0) {
            markFavorites();
          }
        },
        function(error) {
          console.log(error);
        }
      );
    };

    var markFavorites = function() {
      var roomsAndItems = $scope.roomsAndItems;
      ($scope.roomsAsFavorites = {}), ($scope.itemsAsFavorites = {});
      for (var roomId in $scope.roomsAndItems) {
        if (roomsAndItems.hasOwnProperty(roomId)) {
          for (var j = 0; j < $scope.favorites.length; j++) {
            var favorite = $scope.favorites[j];
            if (favorite.userRoomId === roomId && favorite.userRoomItemId === "0") {
              if (
                $scope.roomsAsFavorites[roomId] === undefined ||
                ($scope.roomsAsFavorites[roomId] !== undefined && $scope.roomsAsFavorites[roomId] !== true)
              ) {
                $scope.roomsAsFavorites[roomId] = true;
              }
            } else if (favorite.userRoomId === roomId && favorite.userRoomItemId !== "0") {
              $scope.itemsAsFavorites[roomId + "-" + favorite.userRoomItemId] = true;
            }
          }
        }
      }
      $scope.roomsAndItems = roomsAndItems;
    };

    $scope.saveFavorites = function() {
      var favoriteRoomsChecks = document.querySelectorAll('[name="favoriteRooms"]');
      var favoriteItemsChecks = document.querySelectorAll('[name="favoriteItems"]');
      var favorites = [];
      for (var i = 0; i < favoriteRoomsChecks.length; i++) {
        if (favoriteRoomsChecks[i].checked === true) {
          var favoriteObject = {
            userId: $rootScope.loginDetails.user_id,
            userRoomId: favoriteRoomsChecks[i].value
          };
          favorites.push(favoriteObject);
        }
      }
      for (var i = 0; i < favoriteItemsChecks.length; i++) {
        if (favoriteItemsChecks[i].checked === true) {
          var userRoomDetailsArray = favoriteItemsChecks[i].value.split(":");
          var favoriteObject = {
            userId: $rootScope.loginDetails.user_id,
            userRoomId: userRoomDetailsArray[0],
            userRoomItemId: userRoomDetailsArray[1]
          };
          favorites.push(favoriteObject);
        }
      }

      var postData = UtilityFactory.serializeJSON({
        userId: $rootScope.loginDetails.user_id,
        userFavorites: favorites[0] !== undefined ? favorites : ""
      });
      $http
        .post(environmentVariables.ADORA_API_URL + "/user-favorites", postData, {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        })
        .then(
          function(response) {
            $state.go("tab.favorites");
          },
          function(error) {
            console.log(error);
            $state.go("error", { state: "login" });
          }
        );
    };

    $scope.switch = function(roomIndex, itemIndex) {
      var item = $scope.roomsAndItems[roomIndex][itemIndex];
      var status = $scope.switchedOnDevices[item.userRoomItemId];
      UtilityFactory.createSwitchStatus({
        userId: $rootScope.loginDetails.user_id,
        deviceId: item.deviceId,
        switchNumber: item.switchNumber,
        status: status ? 1 : 0,
        userRoomItemId: item.userRoomItemId
      });
      MQTTService.publish(
        {
          method: "switch",
          device_uid: item.deviceId,
          switch_nos: item.switchNumber,
          action: status ? "ON" : "OFF",
          eventCallback: "switch"
        },
        "a/" + $rootScope.loginDetails.user_email + "/r/req"
      );
    };

    var getSwitchedOnList = function() {
      $http.get(environmentVariables.ADORA_API_URL + "/device-statuses?userId=" + $rootScope.loginDetails.user_id).then(
        function(response) {
          var switchedOnDevices = response.data.switchedOnDevices;
          for (var i = 0; i < switchedOnDevices.length; i++) {
            $scope.switchedOnDevices[switchedOnDevices[i].userRoomItemId] =
              switchedOnDevices[i].status === "1" ? true : false;
          }
        },
        function(response) {
          SocketService.printResponse(response);
        }
      );
    };

    if ($rootScope.loginDetails === undefined) {
      LoginFactory.checkLogin().then(
        function(response) {
          $rootScope.loginDetails = response;
          getRoomsAndItems();
          getSwitchedOnList();
        },
        function(error) {
          $state.go("welcome");
        }
      );
    } else {
      getRoomsAndItems();
      getSwitchedOnList();
    }
  })
  .controller("ClubbedDevicesCtrl", function(
    $scope,
    LoginFactory,
    $rootScope,
    environmentVariables,
    $state,
    $http,
    SocketService,
    UtilityFactory,
    MQTTService,
    $timeout
  ) {
    $scope.showClubbedDevices = ( $scope.switchedOnDevices = ( $scope.switchedOnDevicesReady = ( $scope.clubbedDevicesItemList =  ( $scope.clubbedDeviceStatusSet = false ) ) ) );

    var getSwitchedOnList = function() {
      $http.get(environmentVariables.ADORA_API_URL + "/device-statuses?userId=" + $rootScope.loginDetails.user_id).then(
        function(response) {
          var switchedOnDevices = response.data.switchedOnDevices;
          $scope.switchedOnDevices = {};
          for (var i = 0; i < switchedOnDevices.length; i++) {
            $scope.switchedOnDevices["" + switchedOnDevices[i].userRoomItemId] =
              switchedOnDevices[i].status === "1" ? true : false;
          }
          $scope.switchedOnDevicesReady = true;
          setClubbedDeviceStatus();
        },
        function(response) {
          SocketService.printResponse(response);
        }
      );
    };

    var setClubbedDeviceStatus = function() {
      if ($scope.switchedOnDevicesReady === true && $scope.showClubbedDevices === true && $scope.clubbedDeviceStatusSet === false) {
        $scope.clubbedDeviceStatusSet = true;
        for (var clubbedDeviceId in $scope.clubbedDevices) {
          var userRoomItemIds = $scope.clubbedDevices[clubbedDeviceId].userRoomItemIds.split(",");
          var offCount = 0;
          if ($scope.clubbedDevices[clubbedDeviceId].status == "1") {
            for (var i = 0; i < userRoomItemIds.length; i++) {
              if ($scope.switchedOnDevices[userRoomItemIds[i]] === undefined || $scope.switchedOnDevices[userRoomItemIds[i]] === false) {
                offCount++;
              } else if ($scope.switchedOnDevices[userRoomItemIds[i]] === true) {
                break;
              }
            }
            if (offCount === userRoomItemIds.length) {
              $scope.clubbedDevices[clubbedDeviceId].status = "0";
              var putData = UtilityFactory.serializeJSON({
                status: 0
              });
              $http.put(
                environmentVariables.ADORA_API_URL + "/user-clubbed-devices/" + clubbedDeviceId,
                putData,
                {
                  "headers": {
                    "Content-Type": "application/x-www-form-urlencoded"
                  }
                }
              )
              .then(
                function(response) {
                  console.log(response);
                }, function(error) {
                  console.log(error);
                }
              );
            }
          }
        }
      } else {
        setTimeout(function() {
          setClubbedDeviceStatus();
        }, 10);
      }
    };

    var getClubbedDevices = function() {
      $http
        .get(environmentVariables.ADORA_API_URL + "/user-clubbed-devices?userId=" + $rootScope.loginDetails.user_id)
        .then(
          function(response) {
            var clubbedDevices = ($scope.clubbedDevices = response.data.userClubbedDevices);
            $scope.clubbedDevicesItemList = {};
            if ($.isEmptyObject(clubbedDevices)) {
              $rootScope.verticalMenuOptions = { changeClubbedDevices: true };
            } else {
              $rootScope.verticalMenuOptions = { addClubbedDevices: true };
            }
            UtilityFactory.adjustTileHeight();
            $scope.showClubbedDevices = true;
            setClubbedDeviceStatus();
          },
          function(error) {
            console.log(error);
          }
        );
    };

    $scope.switchClubbedDevice = function(clubbedDeviceId) {
      var status = $scope.clubbedDevices[clubbedDeviceId]["status"];
      var deviceIds = $scope.clubbedDevices[clubbedDeviceId]["deviceIds"];
      var finalObject = {};
      for (var i = 0; i < deviceIds.length; i++) {
        var device = deviceIds[i];
        if (device.device_id.trim() === "") {
          continue;
        }
        if (finalObject[device.device_id] === undefined) {
          finalObject[device.device_id] = device.switch_number;
        } else {
          finalObject[device.device_id] = finalObject[device.device_id] + device.switch_number;
        }
        UtilityFactory.createSwitchStatus({
          userId: $rootScope.loginDetails.user_id,
          deviceId: device.device_id,
          switchNumber: device.switch_number,
          status: status == 1 ? 0 : 1,
          userRoomItemId: device.id
        });
        var putData = UtilityFactory.serializeJSON({
          status: status == 1 ? 0 : 1
        });
      }
      $http.put(
        environmentVariables.ADORA_API_URL + "/user-clubbed-devices/" + clubbedDeviceId,
        putData,
        {
          "headers": {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        }
      )
      .then(
        function(response) {
          console.log(response);
        }, function(error) {
          console.log(error);
        }
      );
      $scope.clubbedDevices[clubbedDeviceId]["status"] = $scope.clubbedDevices[clubbedDeviceId]["status"] == 1 ? 0 : 1;
      for (var deviceId in finalObject) {
        if (finalObject.hasOwnProperty(deviceId)) {
          MQTTService.publish(
            {
              method: "switch",
              device_uid: deviceId,
              switch_nos: finalObject[deviceId],
              action: status == 1 ? "OFF" : "ON",
              eventCallback: "switchClubbedDevice"
            },
            "a/" + $rootScope.loginDetails.user_email + "/r/req"
          );
        }
      }
    };

    var switchSuccess = function(response, error) {
      console.log("switched");
    };

    if ($rootScope.loginDetails === undefined) {
      LoginFactory.checkLogin().then(
        function(response) {
          $rootScope.loginDetails = response;
          getClubbedDevices();
          getSwitchedOnList();
        },
        function(error) {
          $state.go("welcome");
        }
      );
    } else {
      getClubbedDevices();
    }

    $scope.$on("switchClubbedDevice", function() {
      switchSuccess();
    });
  })
  .controller("ClubbedDeviceDetailCtrl", function(
    $scope,
    LoginFactory,
    $rootScope,
    environmentVariables,
    $state,
    $http,
    SocketService,
    $stateParams,
    UtilityFactory,
    MQTTService
  ) {
    $scope.clubbedDeviceId = $stateParams.clubbedDeviceId;
    $scope.clubbedDeviceName = $stateParams.clubbedDeviceName;

    var getSwitchedOnList = function() {
      $http.get(environmentVariables.ADORA_API_URL + "/device-statuses?deviceId=" + $rootScope.loginDetails.user_id).then(
        function(response) {
          var switchedOnDevices = response.data.switchedOnDevices;
          for (var i = 0; i < switchedOnDevices.length; i++) {
            $scope.switchedOnDevices[switchedOnDevices[i].userRoomItemId] =
              switchedOnDevices[i].status === "1" ? true : false;
          }
        },
        function(response) {
          SocketService.printResponse(response);
        }
      );
    };


    var getUserClubbedDeviceItems = function() {
      $http
        .get(
          environmentVariables.ADORA_API_URL +
            "/user-clubbed-devices/" +
            $scope.clubbedDeviceId +
            "?userId=" +
            $rootScope.loginDetails.user_id
        )
        .then(
          function(response) {
            $scope.clubbedDeviceItems = response.data.clubbedDeviceItems;
            $scope.clubbedDeviceName = response.data.name;
            for (var index in $scope.clubbedDeviceItems) {
              if ($scope.clubbedDeviceItems.hasOwnProperty(index)) {
                $scope.clubbedDeviceItems[index]["status"] = 0;
              }
            }
            $rootScope.verticalMenuOptions = { deleteClubbedDevice: true };
          },
          function(error) {
            console.log(error);
          }
        );
    };

    $scope.deleteClubbedDevice = function() {
      $http.delete(environmentVariables.ADORA_API_URL + "/user-clubbed-devices/" + $scope.clubbedDeviceId).then(
        function(response) {
          $state.go("tab.clubbed-devices");
        },
        function(error) {
          console.log(error);
        }
      );
    };

    $scope.switchClubbedDevice = function(index) {
      var item = $scope.clubbedDeviceItems[index];
      UtilityFactory.createSwitchStatus({
        userId: $rootScope.loginDetails.user_id,
        deviceId: item.device_id,
        switchNumber: item.switch_number,
        status: item.status ? 1 : 0
      });
      SocketService.openSocket(switchSuccess, function() {});
      MQTTService.publish(
        {
          method: "switch",
          device_uid: item.device_id,
          switch_nos: item.switch_number,
          action: item.status ? "ON" : "OFF",
          eventCallback: "switchClubbedDeviceDetail"
        },
        "a/" + $rootScope.loginDetails.user_email + "/r/req"
      );
    };

    var switchSuccess = function(response) {
      console.log("switched");
    };

    if ($rootScope.loginDetails === undefined) {
      LoginFactory.checkLogin().then(
        function(response) {
          $rootScope.loginDetails = response;
          getUserClubbedDeviceItems();
        },
        function(error) {
          $state.go("welcome");
        }
      );
    } else {
      getUserClubbedDeviceItems();
    }

    $scope.$on("switchClubbedDeviceDetail", function() {
      switchSuccess();
    });
  })
  .controller("AddClubbedDevicesCtrl", function(
    $scope,
    LoginFactory,
    $rootScope,
    environmentVariables,
    $state,
    $http,
    $ionicPopup,
    $ionicBackdrop,
    UtilityFactory,
    MQTTService
  ) {
    $scope.switchedOnDevices = [];
    $scope.form = { name: "" };

    var getRoomsAndItems = function() {
      $http
        .get(environmentVariables.ADORA_API_URL + "/user-room-items?userId=" + $rootScope.loginDetails.user_id + "&itemInfo=true")
        .then(function(response) {
          if (response.data.status === "success") {
            $scope.roomsAndItems = response.data.roomsAndItems;
            getClubbedDevices();
          }
        });
    };

    var getClubbedDevices = function() {
      $http
        .get(
          environmentVariables.ADORA_API_URL +
            "/user-clubbed-devices?userId=" +
            $rootScope.loginDetails.user_id +
            "&itemInfo=true"
        )
        .then(
          function(response) {
            $scope.clubbedDevices = response.data.userClubbedDevices;
            if ($scope.clubbedDevices.length > 0) {
              markClubbedDevices();
            }
          },
          function(error) {
            console.log(error);
          }
        );
    };

    var markClubbedDevices = function() {};

    $scope.saveClubbedDevices = function() {
      var clubbedRoomsChecks = document.querySelectorAll('[name="clubbedRooms"]');
      var clubbedItemsChecks = document.querySelectorAll('[name="clubbedItems"]');
      var clubbedDevices = [];
      for (var i = 0; i < clubbedRoomsChecks.length; i++) {
        if (clubbedRoomsChecks[i].checked === true) {
          var clubbedRoomObject = {
            userId: $rootScope.loginDetails.user_id,
            userRoomId: clubbedRoomsChecks[i].value
          };
          clubbedDevices.push(clubbedRoomObject);
        }
      }
      for (var i = 0; i < clubbedItemsChecks.length; i++) {
        if (clubbedItemsChecks[i].checked === true) {
          var userRoomDetailsArray = clubbedItemsChecks[i].value.split(":");
          var clubbedRoomObject = {
            userId: $rootScope.loginDetails.user_id,
            userRoomId: userRoomDetailsArray[0],
            userRoomItemId: userRoomDetailsArray[1]
          };
          clubbedDevices.push(clubbedRoomObject);
        }
      }

      var postData = UtilityFactory.serializeJSON({
        userId: $rootScope.loginDetails.user_id,
        name: $scope.form.name,
        userClubbedDevices: !$.isEmptyObject(clubbedDevices) ? clubbedDevices : ""
      });
      $http
        .post(environmentVariables.ADORA_API_URL + "/user-clubbed-devices", postData, {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        })
        .then(
          function(response) {
            $state.go("tab.clubbed-devices");
          },
          function(error) {
            console.log(error);
            $state.go("error", { state: "login" });
          }
        );
    };

    $scope.showPopup = function() {
      $scope.data = {};
      $ionicBackdrop.retain();

      var myPopup = $ionicPopup.show({
        template: '<input type="text" ng-model="form.name" autofocus >',
        title: "Name",
        scope: $scope,
        buttons: [
          {
            text: "Cancel",
            onTap: function(e) {
              $ionicBackdrop.release();
            }
          },
          {
            text: "<b>Done</b>",
            type: "waves-effect waves-light btn-large btn-block",
            onTap: function(e) {
              if ($scope.form.name.trim() === "") {
                //don't allow the user to close unless he enters wifi password
                e.preventDefault();
              } else {
                $scope.saveClubbedDevices();
                $ionicBackdrop.release();
              }
            }
          }
        ]
      });

      myPopup.then(function(res) {});
    };

    $scope.switch = function(roomIndex, itemIndex) {
      var item = $scope.roomsAndItems[roomIndex][itemIndex];
      var status = $scope.switchedOnDevices[item.userRoomItemId];
      UtilityFactory.createSwitchStatus({
        userId: $rootScope.loginDetails.user_id,
        deviceId: item.deviceId,
        switchNumber: item.switchNumber,
        status: status ? 1 : 0,
        userRoomItemId: item.userRoomItemId
      });
      MQTTService.publish(
        {
          method: "switch",
          device_uid: item.deviceId,
          switch_nos: item.switchNumber,
          action: status ? "ON" : "OFF",
          eventCallback: "switch"
        },
        "a/" + $rootScope.loginDetails.user_email + "/r/req"
      );
    };

    var getSwitchedOnList = function() {
      $http.get(environmentVariables.ADORA_API_URL + "/device-statuses?userId=" + $rootScope.loginDetails.user_id).then(
        function(response) {
          var switchedOnDevices = response.data.switchedOnDevices;
          for (var i = 0; i < switchedOnDevices.length; i++) {
            $scope.switchedOnDevices[switchedOnDevices[i].userRoomItemId] =
              switchedOnDevices[i].status === "1" ? true : false;
          }
        },
        function(response) {
          SocketService.printResponse(response);
        }
      );
    };

    if ($rootScope.loginDetails === undefined) {
      LoginFactory.checkLogin().then(
        function(response) {
          $rootScope.loginDetails = response;
          getRoomsAndItems();
          getSwitchedOnList();
        },
        function(error) {
          $state.go("welcome");
        }
      );
    } else {
      getRoomsAndItems();
      getSwitchedOnList();
    }
  })
  .controller("DimmerCtrl", function($scope, SocketService) {
    $scope.dimmer = { id: "", value: 0 };

    $scope.change = function() {
      var data = { method: "dimmer", id: $scope.dimmer.id, value: $scope.dimmer.value };
      SocketService.openSocket(callbackFunction, function() {});
      data = JSON.stringify(data);
      SocketService.writeSocket(data);
    };

    var callbackFunction = function(response) {
      console.log(JSON.stringify(response));
    };
  })
  .controller("RoutineListCtrl", function($scope) {})
  .controller("SwitchedOnListCtrl", function(
    $scope,
    $state,
    $rootScope,
    UtilityFactory,
    LoginFactory,
    $http,
    environmentVariables,
    SocketService,
    MQTTService
  ) {
    var getSwitchedOnList = function() {
      $http.get(environmentVariables.ADORA_API_URL + "/device-statuses?userId=" + $rootScope.loginDetails.user_id).then(
        function(response) {
          $scope.switchedOnDevices = response.data.switchedOnDevices;
          for (var i = 0; i < $scope.switchedOnDevices.length; i++) {
            $scope.switchedOnDevices[i].status = $scope.switchedOnDevices[i].status === "1" ? true : false;
          }
        },
        function(response) {
          SocketService.printResponse(response);
        }
      );
    };

    $scope.switchDevice = function(index) {
      var item = $scope.switchedOnDevices[index];
      UtilityFactory.createSwitchStatus({
        userId: $rootScope.loginDetails.user_id,
        deviceId: item.device_id,
        switchNumber: item.switch_number,
        status: item.status ? 1 : 0,
        userRoomItemId: item.id
      });
      MQTTService.publish(
        {
          method: "switch",
          device_uid: item.device_id,
          switch_nos: item.switch_number,
          action: item.status ? "ON" : "OFF",
          eventCallback: "switchInSwitchedOnList"
        },
        "a/" + $rootScope.loginDetails.user_email + "/r/req"
      );
    };

    var switchSuccess = function(response) {
      SocketService.printResponse(response);
      if (response !== undefined && response.method === "switch" && response.value === "success") {
      } else if (response !== undefined && response.method === "switch" && response.value === "failed") {
        alert(response.dueto);
      } else {
        console.log(error);
        $state.go("error", { state: "login" });
      }
    };

    LoginFactory.checkLogin().then(
      function(response) {
        $rootScope.loginDetails = response;
        getSwitchedOnList();
      },
      function(error) {
        console.log(error);
      }
    );
  })
  .controller("DimmerCtrl", function($scope, SocketService) {
    $scope.dimmer = { id: "", value: 0 };

    $scope.change = function() {
      var data = { method: "dimmer", id: $scope.dimmer.id, value: $scope.dimmer.value };
      SocketService.openSocket(callbackFunction, function() {});
      data = JSON.stringify(data);
      SocketService.writeSocket(data);
    };

    var callbackFunction = function(response) {
      console.log(JSON.stringify(response));
    };
  })
  .controller("PowerConsumptionCtrl", function(
    $scope,
    $ionicPopover,
    $rootScope,
    UtilityFactory,
    SocketService,
    $state,
    $http,
    LoginFactory,
    environmentVariables
  ) {
    $ionicPopover
      .fromTemplateUrl("templates/menupopover.html", {
        scope: $rootScope
      })
      .then(function(popover) {
        $rootScope.menupopover = popover;
      });

    $scope.form = {
      deviceId: "demo-4",
      fromDate: "01/04/2017",
      fromTime: "12:00AM",
      toDate: "05/04/2017",
      toTime: "11:59PM"
    };

    $scope.$on("$ionicView.loaded", function() {
      $("#from-date, #to-date").pickadate({
        format: "dd/mm/yyyy",
        selectMonths: true,
        selectYears: 15
      });
      $("#from-time, #to-time").clockpicker({
        placement: "bottom",
        align: "left",
        twelvehour: true,
        ampmclickable: false
      });
    });

    $scope.checkPowerConsumption = function() {
      $scope.form.fromDate = document.getElementById("from-date").value;
      $scope.form.toDate = document.getElementById("to-date").value;
      if (
        $scope.form.deviceId.trim() === "" ||
        $scope.form.fromDate.trim() === "" ||
        $scope.form.fromDate.trim() === "" ||
        $scope.form.fromTime.trim() === "" ||
        $scope.form.toTime.trim() === ""
      ) {
        alert("Fill all fields");
        return false;
      }

      var fromDateArray = $scope.form.fromDate.split("/");
      var fromTimestamp = new Date(fromDateArray[2] + "/" + fromDateArray[1] + "/" + fromDateArray[0]).getTime() / 1000;
      var toDateArray = $scope.form.toDate.split("/");
      var toTimestamp = new Date(toDateArray[2] + "/" + toDateArray[1] + "/" + toDateArray[0]).getTime() / 1000;

      /*var fromDateArray = new Date($scope.form.fromDate);
    var fromDateString = fromDateArray.getFullYear() + '-' + (fromDateArray.getMonth() + 1) + '-' + fromDateArray.getDate();
    var fromTimeArray = [];
    var fromTimeString = '';
    var toTimeString = '';
    var toTimeArray = [];
    if($scope.form.fromTime.substr($scope.form.fromTime.length -2, 2) === 'PM') {
      fromTimeArray = $scope.form.fromTime.split('PM')[0].split(':');
      fromTimeString = (12 + parseInt(fromTimeArray[0])) + ':' + parseInt(fromTimeArray[1]);
    } else {
      fromTimeArray = $scope.form.fromTime.split('AM')[0].split(':');
      fromTimeString = parseInt(fromTimeArray[0]) + ':' + parseInt(fromTimeArray[1]);
    }
    if($scope.form.toTime.substr($scope.form.toTime.length -2, 2) === 'PM') {
      toTimeArray = $scope.form.toTime.split('PM')[0].split(':');
      toTimeString = (12 + parseInt(toTimeArray[0])) + ':' + parseInt(toTimeArray[1]);
    } else {
      toTimeArray = $scope.form.toTime.split('AM')[0].split(':');
      toTimeString = parseInt(toTimeArray[0]) + ':' + parseInt(toTimeArray[1]);
    }
    var fromTimestamp = UtilityFactory.getTimestampFromDateString(fromDateString + ' ' + fromTimeString + ':00 +0530');

    var toDateArray = new Date($scope.form.toDate);
    var toDateString = toDateArray.getFullYear() + '-' + (toDateArray.getMonth() + 1) + '-' + toDateArray.getDate();
    var toTimestamp = UtilityFactory.getTimestampFromDateString(toDateString + ' ' + toTimeString + ':00 +0530');*/
      var grainSize = $('[name="grainSize"]:checked').val();
      $http
        .get(
          "http://" +
            environmentVariables.DEVICE_API_IP +
            "/p?e=" +
            $rootScope.loginDetails.user_email +
            "&p=" +
            $rootScope.loginDetails.password +
            "&sid=" +
            $scope.form.deviceId +
            "&gt=" +
            fromTimestamp +
            "&lt=" +
            toTimestamp +
            "&gs=" +
            grainSize,
          { headers: { "Content-Type": "application/x-www-form-urlencoded" } }
        )
        .then(
          function(response) {
            console.log(response);
            console.log(typeof response);
            if (
              response !== undefined &&
              response.data !== undefined &&
              response.data.value !== undefined &&
              response.data.value === "success"
            ) {
              var powerData = response.data.data;
              var powerText = "";
              for (var property in powerData) {
                if (powerData.hasOwnProperty(property)) {
                  if (grainSize === "DAILY") {
                    powerText += "Day " + property + ": " + powerData[property] + "\n";
                  } else if (grainSize === "WEEKLY") {
                    powerText += "Week " + property + ": " + powerData[property] + "\n";
                  } else {
                    powerText += "Month " + property + ": " + powerData[property] + "\n";
                  }
                }
              }
              if (powerText !== "") {
                alert(powerText);
              }
            } else if (response.value !== undefined && response.value === "failed") {
              if (response.dueto === "device not associated with account") {
                alert("Wrong device id");
              }
            }
          },
          function(response) {
            SocketService.printResponse(response);
          }
        );
    };

    var powerConsumptionCallback = function(response) {
      SocketService.printResponse(response);
      if (response !== undefined && response.method === "getPwrConsumption" && response.value === "success") {
      } else if (response !== undefined && response.method === "getPwrConsumption" && response.value !== "") {
        alert(response.dueto);
      } else {
        $state.go("error", { state: "login" });
      }
    };

    LoginFactory.checkLogin().then(
      function(response) {
        $rootScope.loginDetails = response;
      },
      function(error) {
        console.log(error);
      }
    );
  })
  .controller("AboutCtrl", function($scope, $state, $rootScope, LoginFactory, SocketService, $stateParams) {
    $scope.$on("$ionicView.loaded", function() {
      var windowHeight = $(window).height() / 2 - 69;
      $(".tile-item").height(windowHeight);
    });
  })
  .controller("RoutinesCtrl", function(
    $scope,
    $state,
    $rootScope,
    LoginFactory,
    SocketService,
    $stateParams,
    environmentVariables,
    $http,
    UtilityFactory,
    RoutineService,
    $ionicActionSheet,
    $timeout
  ) {
    var getRoutines = function() {
      $http.get(environmentVariables.ADORA_API_URL + "/user-routines?userId=" + $rootScope.loginDetails.user_id).then(
        function(response) {
          $rootScope.verticalMenuOptions = { addRoutine: true };
          if (response.data.routines !== undefined) {
            $scope.routines = response.data.routines;
            $scope.showRoutines = true;
            UtilityFactory.adjustTileHeight();
          }
        },
        function(error) {
          console.log(error);
          $state.go("error");
        }
      );
    };

    var deleteUserRoutine = function(routineId) {
      $http.delete(environmentVariables.ADORA_API_URL + "/user-routines/" + routineId).then(
        function(response) {
          $state.go("tab.routines", {}, { reload: true });
        },
        function(error) {
          console.log(error);
        }
      );
    };

    $scope.onHold = function(itemIndex) {
      var hideSheet = $ionicActionSheet.show({
        destructiveText: "Delete",
        titleText: $scope.routines[itemIndex].name,
        cancelText: "Cancel",
        destructiveButtonClicked: function() {
          deleteUserRoutine($scope.routines[itemIndex].id);
        }
      });

      $timeout(function() {
        hideSheet();
      }, 10000);
    };

    if ($rootScope.loginDetails === undefined) {
      LoginFactory.checkLogin().then(
        function(response) {
          $rootScope.loginDetails = response;
          RoutineService.resetRoutine();
          getRoutines();
        },
        function(error) {
          $state.go("welcome");
        }
      );
    } else {
      getRoutines();
    }
  })
  .controller("AddRoutineCtrl", function(
    $scope,
    $state,
    $rootScope,
    LoginFactory,
    SocketService,
    $stateParams,
    environmentVariables,
    $http,
    $ionicBackdrop,
    $ionicPopup,
    RoutineService
  ) {
    $scope.form = { name: "" };

    var getRoomsAndItems = function() {
      $http
        .get(environmentVariables.ADORA_API_URL + "/user-room-items?userId=" + $rootScope.loginDetails.user_id)
        .then(function(response) {
          if (response.data.status === "success") {
            $scope.roomsAndItems = response.data.roomsAndItems;
          }
        });
    };

    $scope.saveToRoutineService = function() {
      var routineRoomsChecks = document.querySelectorAll('[name="routineRooms"]');
      var routineItemsChecks = document.querySelectorAll('[name="routineItems"]');
      var routineDevices = [];
      for (var i = 0; i < routineRoomsChecks.length; i++) {
        if (routineRoomsChecks[i].checked === true) {
          var routineRoomObject = {
            userId: $rootScope.loginDetails.user_id,
            userRoomId: routineRoomsChecks[i].value
          };
          routineDevices.push(routineRoomObject);
        }
      }
      for (var i = 0; i < routineItemsChecks.length; i++) {
        if (routineItemsChecks[i].checked === true) {
          var userRoomDetailsArray = routineItemsChecks[i].value.split(":");
          var routineRoomObject = {
            userId: $rootScope.loginDetails.user_id,
            userRoomId: userRoomDetailsArray[0],
            userRoomItemId: userRoomDetailsArray[1]
          };
          routineDevices.push(routineRoomObject);
        }
      }

      if (!$.isEmptyObject(routineDevices)) {
        RoutineService.saveDevices(routineDevices);
        $state.go("tab.routine-timings");
      } else {
        alert("Choose Devices");
      }
    };

    $scope.saveRoutine = function() {
      var postData = UtilityFactory.serializeJSON({
        userId: $rootScope.loginDetails.user_id,
        name: $scope.form.name,
        routine: !$.isEmptyObject(routineDevices) ? routineDevices : ""
      });
      $http
        .post(environmentVariables.ADORA_API_URL + "/user-routines", postData, {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        })
        .then(
          function(response) {
            $state.go("tab.routines");
          },
          function(error) {
            console.log(error);
            $state.go("error", { state: "login" });
          }
        );
    };

    if ($rootScope.loginDetails === undefined) {
      LoginFactory.checkLogin().then(
        function(response) {
          $rootScope.loginDetails = response;
          getRoomsAndItems();
        },
        function(error) {
          $state.go("welcome");
        }
      );
    } else {
      getRoomsAndItems();
    }
  })
  .controller("RoutineChoiceCtrl", function(
    $scope,
    $state,
    $rootScope,
    LoginFactory,
    SocketService,
    $stateParams,
    environmentVariables,
    $http,
    $ionicBackdrop,
    $ionicPopup
  ) {
    $scope.showNameDialog = $stateParams.showDialog;

    $scope.form = { name: "" };

    if ($rootScope.loginDetails === undefined) {
      LoginFactory.checkLogin().then(
        function(response) {
          $rootScope.loginDetails = response;
        },
        function(error) {
          $state.go("welcome");
        }
      );
    } else {
    }
  })
  .controller("RoutineDetailCtrl", function(
    $scope,
    $state,
    $rootScope,
    LoginFactory,
    SocketService,
    $stateParams,
    environmentVariables,
    $http,
    $ionicBackdrop,
    $ionicPopup
  ) {
    $scope.routineId = $stateParams.routineId;
    $scope.routine = {
      id: $stateParams.id,
      name: $stateParams.routineName,
      onTime: false,
      offTime: false,
      days: "",
      userRoomItems: []
    };
    $scope.showRoutine = false;
    $rootScope.verticalMenuOptions = { deleteUserRoutine: true };

    var getRoutine = function() {
      $http.get(environmentVariables.ADORA_API_URL + "/user-routines/" + $scope.routineId).then(
        function(response) {
          if (response.data.status === "success") {
            var routine = response.data.routine;
            if (routine.userRoomItemIds.trim() === "") {
            } else {
              $scope.routine.onTime = routine.onTime;
              $scope.routine.offTime = routine.offTime;
            }
            var days = routine.days.split(",");
            for (var i = 0; i < $scope.routine.days.length; i++) {
              $scope.routine.days[i].isSelected = days.indexOf(i.toString()) > -1 ? true : false;
            }
            $scope.userRoomItems = routine.deviceIds;
            $scope.showRoutine = true;
          }
        },
        function(error) {
          console.log(error);
          $state.go("error");
        }
      );
    };

    $scope.deleteUserRoutine = function() {
      $http.delete(environmentVariables.ADORA_API_URL + "/user-routines/" + $scope.routineId).then(
        function(response) {
          $state.go("tab.routines", {}, { reload: true });
        },
        function(error) {
          console.log(error);
        }
      );
    };

    if ($rootScope.loginDetails === undefined) {
      LoginFactory.checkLogin().then(
        function(response) {
          $rootScope.loginDetails = response;
          getRoutine();
        },
        function(error) {
          $state.go("welcome");
        }
      );
    } else {
      getRoutine();
    }
  })
  .controller("RoutineTimingsCtrl", function(
    $scope,
    $state,
    $rootScope,
    LoginFactory,
    SocketService,
    $stateParams,
    environmentVariables,
    $http,
    $ionicBackdrop,
    $ionicPopup,
    RoutineService,
    UtilityFactory
  ) {
    $scope.timings = { onTime: "", offTime: "", days: "" };

    $scope.form = { name: "" };

    $scope.saveRoutine = function() {
      if ($scope.form.name.trim() !== "") {
        var days = [];
        for (var index in $scope.timings.days) {
          if ($scope.timings.days[index].isSelected === true) {
            days.push(index);
          }
        }
        days = days.join(",");
        var postData = UtilityFactory.serializeJSON({
          userId: $rootScope.loginDetails.user_id,
          name: $scope.form.name,
          userRoutineDevices: !$.isEmptyObject(RoutineService.routine.devices) ? RoutineService.routine.devices : "",
          onTime: $scope.timings.onTime,
          offTime: $scope.timings.offTime,
          days: days
        });
        $http
          .post(environmentVariables.ADORA_API_URL + "/user-routines", postData, {
            headers: {
              "Content-Type": "application/x-www-form-urlencoded"
            }
          })
          .then(
            function(response) {
              $state.go("tab.routines");
            },
            function(error) {
              console.log(error);
              $state.go("error", { state: "login" });
            }
          );
      }
    };

    $scope.showPopup = function() {
      $scope.data = {};
      $scope.timings.onTime = document.getElementById("on-time").value;
      $scope.timings.offTime = document.getElementById("off-time").value;
      if ($scope.timings.onTime.trim() === "" && $scope.timings.offTime.trim() === "") {
        alert("Please enter on/off time");
        return false;
      }

      $ionicBackdrop.retain();
      var myPopup = $ionicPopup.show({
        template: '<input type="text" ng-model="form.name" autofocus >',
        title: "Name",
        scope: $scope,
        buttons: [
          {
            text: "Cancel",
            onTap: function(e) {
              $ionicBackdrop.release();
            }
          },
          {
            text: "<b>Done</b>",
            type: "waves-effect waves-light btn-large btn-block",
            onTap: function(e) {
              if ($scope.form.name.trim() === "") {
                e.preventDefault();
              } else {
                $scope.saveRoutine();
                $ionicBackdrop.release();
              }
            }
          }
        ]
      });

      myPopup.then(function(res) {});
    };

    $scope.$on("$ionicView.loaded", function() {
      $("#on-time, #off-time").clockpicker({
        placement: "bottom",
        align: "left",
        twelvehour: true,
        ampmclickable: false
      });
    });
  });
