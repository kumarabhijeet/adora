angular
  .module("adora.factories", [])
  .factory("LoginFactory", function($state, SQLiteService, $q, $cordovaSQLite) {
    var loginDetails = {};

    var checkLogin = function() {
      var loggedInQuery = "SELECT id, user_id, user_email, password FROM user_info WHERE status = 1";
      var defer = $q.defer();
      if (window.cordova) {
        $cordovaSQLite.execute(SQLiteService.db, loggedInQuery).then(
          function(result) {
            if (result.rows.length > 0) {
              loginDetails = result.rows.item(0);
              defer.resolve(loginDetails);
            } else {
              defer.reject();
            }
          },
          function(err) {
            defer.reject();
          }
        );
        return defer.promise;
      } else {
        SQLiteService.db.transaction(function(tx) {
          tx.executeSql(
            loggedInQuery,
            [],
            function(tx, response) {
              loginDetails = response.rows[0];
              defer.resolve(loginDetails);
            },
            function(tx, error) {
              defer.reject();
            }
          );
        });
        return defer.promise;
      }
    };

    return {
      checkLogin: checkLogin,
      loginDetails: loginDetails
    };
  })
  .factory("RestFactory", function($http, environmentVariables, $q) {})
  .factory("UtilityFactory", function($http, environmentVariables) {
    var getTimestampFromDateString = function(dateString) {
      var date = new Date(dateString);
      return date.getTime();
    };

    /**
   *  This function converts a JSON into serialized string to be sent via post or put $http calls
   */
    var serializeJSON = function(obj) {
      var query = "",
        name,
        value,
        fullSubName,
        subName,
        subValue,
        innerObj,
        i;

      for (name in obj) {
        value = obj[name];

        if (value instanceof Array) {
          for (i = 0; i < value.length; ++i) {
            subValue = value[i];
            fullSubName = name + "[" + i + "]";
            innerObj = {};
            innerObj[fullSubName] = subValue;
            query += serializeJSON(innerObj) + "&";
          }
        } else if (value instanceof Object) {
          for (subName in value) {
            subValue = value[subName];
            fullSubName = name + "[" + subName + "]";
            innerObj = {};
            innerObj[fullSubName] = subValue;
            query += serializeJSON(innerObj) + "&";
          }
        } else if (value !== undefined && value !== null) {
          query += encodeURIComponent(name) + "=" + encodeURIComponent(value) + "&";
        }
      }

      return query.length ? query.substr(0, query.length - 1) : query;
    };

    var createSwitchStatus = function(switchInfo) {
      var postData =
        "userId=" +
        switchInfo.userId +
        "&deviceId=" +
        switchInfo.deviceId +
        "&switchNumber=" +
        switchInfo.switchNumber +
        "&status=" +
        switchInfo.status +
        "&userRoomItemId=" +
        switchInfo.userRoomItemId;
      $http
        .post(environmentVariables.ADORA_API_URL + "/device-statuses", postData, {
          headers: { "Content-Type": "application/x-www-form-urlencoded" }
        })
        .then(
          function(response) {
            //console.log(response);
          },
          function(error) {
            console.log(error);
          }
        );
    };

    var adjustTileHeight = function() {
      setTimeout(function() {
        var windowHeight = $(window).height() / 2 - 69;
        $(".tile-item").height(windowHeight);
      }, 0);
    };

    return {
      getTimestampFromDateString: getTimestampFromDateString,
      serializeJSON: serializeJSON,
      createSwitchStatus: createSwitchStatus,
      adjustTileHeight: adjustTileHeight
    };
  });
