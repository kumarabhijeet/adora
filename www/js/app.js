angular
  .module("adora", [
    "ionic",
    "ngCordova",
    "adora.controllers",
    "adora.services",
    "adora.factories",
    "adora.directives",
    "ng-weekday-selector"
  ])
  .constant("environmentVariables", {
    DEVICE_API_IP: "adora.wemake.tech",
    DEVICE_API_SERVER_PORT: 1884,
    ADORA_API_URL: "http://ec2-34-228-178-158.compute-1.amazonaws.com"
    //ADORA_API_URL: "http://192.168.0.105/adora/web"
    //ADORA_API_URL: "http://localhost/adora/web"
  })
  .run(function(
    $ionicPlatform,
    $rootScope,
    LoginFactory,
    SocketService,
    $state,
    SQLiteService,
    $cordovaSQLite,
    environmentVariables,
    MQTTService
  ) {
    $ionicPlatform.ready(function() {
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }

      var state;

      SQLiteService.setDb();
      if (window.cordova) {
        SQLiteService.checkLoginOnDeviceReady().then(
          function(loginDetails) {
            MQTTService.connect(loginDetails.user_email, loginDetails.password, true);
          },
          function(error) {
            console.log(error);
          }
        );
      } else {
        SQLiteService.checkLoginOnDeviceReady().then(
          function(loginDetails) {
            MQTTService.connect(loginDetails.user_id, loginDetails.password, true);
          },
          function(error) {
            console.log(error);
          }
        );
      }

      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }

      var callback = function(data) {
        if ((data !== undefined && data.method === "login" && data.value === "success") || !window.cordova) {
          $state.go("tab.home");
        }
      };

      var resume = function() {
        $state.go(state.toState, state.toParams);
      };

      document.addEventListener("resume", resume, false);

      $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
        state = { toState: toState.name, toParams: toParams, fromState: fromState.name, fromParams: fromParams };
      });
    });
  })
  .config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider) {
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $stateProvider
      .state("welcome", {
        url: "/welcome",
        templateUrl: "templates/welcome.html",
        controller: "LoginCtrl"
      })
      .state("login", {
        url: "/login",
        templateUrl: "templates/login.html",
        controller: "LoginCtrl"
      })
      .state("signup", {
        url: "/signup",
        templateUrl: "templates/signup.html",
        controller: "SignUpCtrl"
      })
      .state("error", {
        url: "/error/:state",
        params: {
          state: ""
        },
        templateUrl: "templates/error.html",
        controller: "SignUpCtrl"
      })
      .state("tab", {
        url: "/tab",
        abstract: true,
        templateUrl: "templates/tabs.html",
        controller: "LayoutCtrl"
      })
      .state("tab.home", {
        url: "/home",
        views: {
          "tab-home": {
            templateUrl: "templates/home.html",
            controller: "HomeCtrl"
          }
        }
      })
      .state("tab.add-room", {
        url: "/add-room",
        views: {
          "tab-home": {
            templateUrl: "templates/add-room.html",
            controller: "AddRoomCtrl"
          }
        }
      })
      .state("tab.room-detail", {
        url: "/room-detail/:userRoomId/:userRoomName",
        params: {
          userRoomId: "",
          userRoomName: ""
        },
        views: {
          "tab-home": {
            templateUrl: "templates/room-detail.html",
            controller: "RoomDetailCtrl"
          }
        }
      })
      .state("settings", {
        url: "/settings",
        templateUrl: "templates/signup.html",
        controller: "SignUpCtrl"
      })
      .state("tab.about", {
        url: "/about",
        views: {
          "tab-home": {
            templateUrl: "templates/appliances.html",
            controller: "AboutCtrl"
          }
        }
      })
      .state("terms", {
        url: "/terms",
        templateUrl: "templates/signup.html",
        controller: "SignUpCtrl"
      })
      .state("privacy", {
        url: "/privacy",
        templateUrl: "templates/signup.html",
        controller: "SignUpCtrl"
      })
      .state("tab.room-items", {
        url: "/room-items/:userRoomId",
        params: {
          userRoomId: ""
        },
        views: {
          "tab-home": {
            templateUrl: "templates/room-items.html",
            controller: "RoomItemsCtrl"
          }
        }
      })
      .state("tab.map-devices", {
        url: "/map-devices",
        params: {
          itemId: null,
          itemName: null,
          userRoomId: null
        },
        views: {
          "tab-home": {
            templateUrl: "templates/map-devices.html",
            controller: "MapDevicesCtrl"
          }
        }
      })
      .state("tab.fill-room", {
        url: "/fill-room/:userRoomId",
        params: {
          userRoomId: ""
        },
        views: {
          "tab-home": {
            templateUrl: "templates/fill-room.html",
            controller: "FillRoomCtrl"
          }
        }
      })
      .state("tab.item-detail", {
        url: "/item-detail/:userRoomItemId/:userRoomItemName",
        cache: false,
        params: {
          userRoomItemId: "",
          userRoomItemName: ""
        },
        views: {
          "tab-home": {
            templateUrl: "templates/item-detail.html",
            controller: "ItemDetailCtrl"
          }
        }
      })
      .state("tab.favorites", {
        url: "/favorites",
        views: {
          "tab-favorites": {
            templateUrl: "templates/favorites.html",
            controller: "FavoritesCtrl"
          }
        }
      })
      .state("tab.add-favorite", {
        url: "/add-favorite",
        cache: false,
        views: {
          "tab-favorites": {
            templateUrl: "templates/add-favorite.html",
            controller: "AddFavoriteCtrl"
          }
        }
      })
      .state("tab.favorite-room-detail", {
        url: "/favorite-room-detail/:userRoomId/:userRoomName",
        params: {
          userRoomId: "",
          userRoomName: ""
        },
        views: {
          "tab-favorites": {
            templateUrl: "templates/room-detail.html",
            controller: "RoomDetailCtrl"
          }
        }
      })
      .state("tab.clubbed-devices", {
        url: "/clubbed-devices",
        cache: false,
        views: {
          "tab-clubbed": {
            templateUrl: "templates/clubbed-devices.html",
            controller: "ClubbedDevicesCtrl"
          }
        }
      })
      .state("tab.clubbed-device-detail", {
        url: "/clubbed-device-detail/:clubbedDeviceId/:clubbedDeviceName",
        cache: false,
        views: {
          "tab-clubbed": {
            templateUrl: "templates/clubbed-device-detail.html",
            controller: "ClubbedDeviceDetailCtrl"
          }
        }
      })
      .state("tab.add-clubbed-devices", {
        url: "/add-clubbed-devices",
        views: {
          "tab-clubbed": {
            templateUrl: "templates/add-clubbed-devices.html",
            controller: "AddClubbedDevicesCtrl"
          }
        }
      })
      .state("power-consumption", {
        url: "/power-consumption",
        templateUrl: "templates/power-consumption.html",
        controller: "PowerConsumptionCtrl"
      })
      .state("switched-on-list", {
        url: "/switched-on-list",
        templateUrl: "templates/switched-on-list.html",
        controller: "SwitchedOnListCtrl"
      })
      .state("tab.dimmer", {
        url: "/dimmer",
        views: {
          "tab-favorites": {
            templateUrl: "templates/dimmer.html",
            controller: "DimmerCtrl"
          }
        }
      })
      .state("tab.routines", {
        url: "/routines",
        views: {
          "tab-routines": {
            templateUrl: "templates/routines.html",
            controller: "RoutinesCtrl"
          }
        }
      })
      .state("tab.add-routine", {
        url: "/add-routines",
        views: {
          "tab-routines": {
            templateUrl: "templates/add-routine.html",
            controller: "AddRoutineCtrl"
          }
        }
      })
      .state("tab.routine-choice", {
        url: "/routine-choice",
        views: {
          "tab-routines": {
            templateUrl: "templates/routine-choice.html",
            controller: "RoutineChoiceCtrl"
          }
        }
      })
      .state("tab.routine-detail", {
        url: "/routine-detail/:routineId/:routineName",
        views: {
          "tab-routines": {
            templateUrl: "templates/routine-detail.html",
            controller: "RoutineDetailCtrl"
          }
        }
      })
      .state("tab.routine-timings", {
        url: "/routine-timings",
        views: {
          "tab-routines": {
            templateUrl: "templates/routine-timings.html",
            controller: "RoutineTimingsCtrl"
          }
        }
      });
    $ionicConfigProvider.platform.android.tabs.position("bottom");
    $urlRouterProvider.otherwise("/welcome");
  });
