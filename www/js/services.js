angular
  .module("adora.services", [])
  .service("SocketService", function($rootScope) {
    this.socket = "";

    var receiveData = function(data) {
      var chars = new Array(data.length);
      for (var i = 0; i < data.length; i++) {
        chars.push(String.fromCharCode(data[i]));
      }
      var dataString = chars.join("");
      var data = JSON.parse(dataString);
      return data;
    };

    this.openSocket = function(onDataCallback, onErrorCallback) {
      if (window.cordova) {
        this.socket.onData = function(data) {
          data = receiveData(data);
          $rootScope.$apply(function() {
            onDataCallback(data);
          });
        };

        this.socket.onError = function(data) {
          data = receiveData(data);
          $rootScope.$apply(function() {
            onErrorCallback(e);
          });
        };

        this.socket.onClose = function(hasError) {
          console.info("Socket closed, hasErrors=" + hasError);
          //setDisconnected();
        };
      } else {
        //
      }
    };

    this.writeSocket = function(jsonData) {
      if (window.cordova) {
        var bytes = new Uint8Array(jsonData.length + 1);
        for (var i = 0; i < jsonData.length; i++) {
          bytes[i] = jsonData.charCodeAt(i);
        }
        bytes[jsonData.length] = "\n".charCodeAt(0);
        this.socket.write(bytes, function() {
          /*alert("data written");*/
        });
      }
    };

    this.printResponse = function(data) {
      console.log(JSON.stringify(data));
    };

    this.closeSocket = function() {
      if (window.cordova) {
        this.socket.close();
      }
    };
  })
  .service("SQLiteService", function($cordovaSQLite, $q) {
    this.db = "";

    this.setDb = function() {
      if (window.cordova) {
        this.db = $cordovaSQLite.openDB({ name: "adora.db", location: "default" });
        this.createTables();
      } else {
        this.db = openDatabase("adora.db", "1.0", "Adora DB", 2 * 1024 * 1024);
        this.createTables();
      }
    };

    this.getDb = function() {
      return this.db;
    };

    this.insert = function(query, params) {
      $cordovaSQLite.execute(this.db, query, params).then(
        function(result) {
          console.log(JSON.stringify(result));
          return true;
        },
        function(error) {
          console.error(JSON.stringify(error));
          return false;
        }
      );
    };

    this.execute = function(query, params) {
      if (window.cordova) {
        $cordovaSQLite.execute(this.db, query, params).then(
          function(result) {
            console.log(JSON.stringify(result));
          },
          function(error) {
            console.log(JSON.stringify(error));
          }
        );
      } else {
        this.db.transaction(function(tx) {
          tx.executeSql(
            query,
            params,
            function(tx, response) {
              console.log(response);
            },
            function(tx, error) {
              console.log(error);
            }
          );
        });
      }
    };

    this.createTables = function() {
      if (window.cordova) {
        var query =
          "CREATE TABLE IF NOT EXISTS user_info (id INT(11) PRIMARY KEY , user_id INT(11), user_email TEXT(200), password TEXT(200), status INT(1))";
        $cordovaSQLite.execute(this.db, query).then(
          function(response) {
            console.log("Creating tables success : ");
            console.log(JSON.stringify(response));
          },
          function(error) {
            console.log("Creating tables error : ");
            console.log(JSON.stringify(error));
          }
        );
      } else {
        var query =
          "CREATE TABLE IF NOT EXISTS user_info (id INTEGER PRIMARY KEY, user_id INTEGER, user_email TEXT, password TEXT, status INTEGER)";
        this.db.transaction(function(tx) {
          tx.executeSql(
            query,
            [],
            function(tx, response) {
              console.log("Creating tables success : ");
            },
            function(tx, error) {
              console.log("Creating tables error : ");
              console.log(JSON.stringify(error));
            }
          );
        });
      }
    };

    this.checkLoginOnDeviceReady = function() {
      var loggedInQuery = "SELECT * FROM user_info WHERE status = 1";
      var defer = $q.defer();
      if (window.cordova) {
        $cordovaSQLite.execute(this.db, loggedInQuery).then(
          function(result) {
            if (result.rows.length > 0) {
              defer.resolve(result.rows.item(0));
            } else {
              defer.reject();
            }
          },
          function(error) {
            console.log(JSON.stringify(error));
            defer.reject(error);
          }
        );
      } else {
        var thisObj = this;
        this.db.transaction(function(tx) {
          tx.executeSql(
            loggedInQuery,
            [],
            function(tx, response) {
              if (response.rows.length === 0) {
                if (
                  thisObj.insert(
                    "INSERT INTO user_info (id, user_id, user_email, password, status) VALUES (NULL, ?, ?, ?, 1)",
                    [1, "dd", "adora_test"]
                  ) !== false
                ) {
                  defer.resolve({ user_id: "dd", password: "adora_test" });
                } else {
                  defer.reject(response);
                }
              } else {
                defer.resolve({ user_id: "dd", password: "adora_test" });
              }
            },
            function(tx, error) {
              defer.reject(error);
            }
          );
        });
      }
      return defer.promise;
    };

    this.logout = function() {
      var logoutQuery = "UPDATE user_info SET status = 0 WHERE status = 1";
      var defer = $q.defer();
      $cordovaSQLite.execute(this.db, logoutQuery).then(
        function(result) {
          console.log(result);
          if (1) {
            defer.resolve();
          } else {
            defer.reject();
          }
        },
        function(error) {
          console.log(JSON.stringify(error));
          defer.reject();
        }
      );
      return defer.promise;
    };
  })
  .service("MQTTService", function($rootScope, environmentVariables, $q, $state) {
    var client = "";
    var callbacksLookup = {
      login: "001",
      attach: "002",
      switch: "003",
      bind: "004",
      switchInItemDetail: "005",
      switchFavorites: "006",
      switchClubbedDevice: "007",
      switchClubbedDeviceDetail: "008",
      switchInSwitchedOnList: "009"
    };
    var count = 0;

    var swap = function(json) {
      var ret = {};
      for (var key in json) {
        ret[json[key]] = key;
      }
      return ret;
    };

    var reverseCallbacksLookup = swap(callbacksLookup);

    this.onConnectionLost = function(error) {
      console.log(JSON.stringify(error));
      console.log("Device server connection lost");
    };

    this.onMessageArrived = function(message) {
      var payload = JSON.parse(message.payloadString);
      console.log(typeof payload === "object" && window.cordova ? JSON.stringify(payload) : payload);
      if (
        payload !== undefined &&
        typeof payload === "object" &&
        payload.eventCallback !== undefined &&
        payload.eventCallback !== "000"
      ) {
        $rootScope.$broadcast(reverseCallbacksLookup[payload.eventCallback]);
        //$state.go("home");
      } else {
        console.log(payload);
        console.log("No event callback");
      }
    };

    this.connect = function(userName, password, alreadyLoggedIn) {
      var onConnect = function(data) {
        console.log("Connected to MQTT Server");
        $rootScope.$broadcast("login");
        client.subscribe("a/" + userName + "/r/res", {
          qos: 1,
          onSuccess: function(data) {
            console.log("Subscribed");
          }
        });
        if (alreadyLoggedIn === true) {
          //$state.go("tab.home");
        }
      };
      client = new Paho.MQTT.Client(
        environmentVariables.DEVICE_API_IP,
        Number(environmentVariables.DEVICE_API_SERVER_PORT),
        "clientId"
      );
      client.onConnectionLost = this.onConnectionLost;
      client.onMessageArrived = this.onMessageArrived;
      if (userName !== undefined) {
        client.connect({ onSuccess: onConnect, userName: "a" + userName, password: password });
      } else {
        console.log(client);
        client.connect({ onSuccess: onConnect });
      }
    };

    this.publish = function(data, destinationName) {
      if (data !== undefined && data.eventCallback !== undefined) {
        data.eventCallback = callbacksLookup[data.eventCallback];
      }
      console.log(typeof data === "object" && window.cordova ? JSON.stringify(data) : data);
      var message = new Paho.MQTT.Message(JSON.stringify(data));
      message.destinationName = destinationName;
      client.send(message);
    };
  })
  .service("RoutineService", function() {
    this.routineDefault = { devices: [], onTime: "", offTime: "" };

    this.routine = this.routineDefault;

    this.resetRoutine = function() {
      this.routine = this.routineDefault;
    };

    this.saveDevices = function(devices) {
      this.routine.devices = devices;
    };

    this.saveTimings = function(timings) {
      this.routine.onTime = timings.onTime;
      this.routine.offTime = timings.offTime;
    };
  })
  .service("LocalSyncService", function() {});
